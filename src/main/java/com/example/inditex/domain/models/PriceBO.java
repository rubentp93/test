package com.example.inditex.domain.models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

import java.time.Instant;

@Builder
@Data
public class PriceBO {
    Long id;
    long brandId;
    long productId;
    long priceListId;
    BigDecimal price;
    int priority;
    CurrencyType coin;

    Instant startTime;
    Instant endTime;

}
