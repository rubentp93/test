package com.example.inditex.domain.models;

import lombok.Getter;

@Getter
public enum CurrencyType
{
    EUR("EUR"),GBP("GBP"),USD("USD");

    private final String value;

    CurrencyType(String value){
        this.value = value;
    }
}
