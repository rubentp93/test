package com.example.inditex.domain.service;

import com.example.inditex.domain.models.PriceBO;
import org.springframework.stereotype.Service;

@Service
public interface PriceService {

    PriceBO getPriceByDate(long brandId, long productId, String date, String coin, String format) ;

}
