package com.example.inditex.domain.service.implemtents;

import com.example.inditex.domain.exceptions.NotFoundException;
import com.example.inditex.domain.exceptions.NotValidInputException;
import com.example.inditex.domain.models.CurrencyType;
import com.example.inditex.domain.models.PriceBO;
import com.example.inditex.domain.service.PriceService;

import com.example.inditex.infrastructure.persistance.PriceStoreAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Component
public class PriceServiceImpl implements PriceService {

    @Autowired
    PriceStoreAdapter priceStoreAdapter;

    @Override
    public PriceBO getPriceByDate(long brandId, long productId, String date, String coin, String format) {

        validateCurrency(coin);
        long epoch = Objects.equals(format, "date") ? stringDateToEpocLong(date) : stringEpocToEpocLong(date);
        return getPriceByTimeStamp(brandId, productId, epoch, coin);

    }



    // ------------------------------------------------------------------------------------------

    private PriceBO getPriceByTimeStamp(long brandId, long productId, long moment, String coin) {
        Instant momentInstant = Instant.ofEpochSecond(moment);
        return priceStoreAdapter.getPrices(brandId, productId, coin).stream()
                .filter((item) -> (item.getStartTime().isBefore(momentInstant) || item.getStartTime().equals(momentInstant) ) && item.getEndTime().isAfter(momentInstant))
                .max(Comparator.comparingInt(PriceBO::getPriority)).orElseThrow(() -> new NotFoundException("Not found price"));
    }

    private long stringEpocToEpocLong(String date) {
        long epoch;
        try {
            epoch = Long.parseLong(date);
        } catch (NumberFormatException e) {
            throw new NotValidInputException("Date introduced is not valid. Should be a number");
        }
        return epoch;
    }

    private long stringDateToEpocLong(String date) {
        long epocSec;
        SimpleDateFormat sdfUtc = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        sdfUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
           Date dateUtc = sdfUtc.parse(date);
           epocSec = dateUtc.getTime() / 1000;
        } catch (ParseException e) {
            throw new NotValidInputException("Date introduced is not valid. Should follow this pattern yyyy-MM-dd-HH.mm.ss");
        }
        return epocSec;
    }

    private void validateCurrency(String coin) {
        if(Arrays.stream(CurrencyType.values()).noneMatch((currency)->currency.getValue().equals(coin))){
            throw new NotValidInputException("Currency introduced is not valid.");
        }
    }



}
