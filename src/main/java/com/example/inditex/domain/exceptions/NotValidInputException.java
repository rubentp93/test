package com.example.inditex.domain.exceptions;

public class NotValidInputException extends RuntimeException {

    public NotValidInputException() {
    }

    public NotValidInputException(String message) {
        super(message);
    }

    public NotValidInputException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotValidInputException(Throwable cause) {
        super(cause);
    }

}
