package com.example.inditex.infrastructure.controller;

import com.example.inditex.infrastructure.controller.Adapter.PriceServiceFacadeAdapter;
import com.example.inditex.infrastructure.controller.Models.ItemPriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/price")
public class PriceController {

    @Autowired
    PriceServiceFacadeAdapter priceServiceFacadeAdapter;

    //Example api/price/brands/zara/products/12345678?moment=12334123&coin=EUR&format:epoc date;

    @GetMapping( "brands/{brandId}/products/{productId}")
    public ItemPriceDTO getPriceForItem(@PathVariable(value="brandId") long brandId ,
                                        @PathVariable(value="productId") long productId,
                                        @RequestParam String date ,@RequestParam(defaultValue = "EUR") String coin,
                                        @RequestParam(defaultValue = "epoc") String format) {
        return priceServiceFacadeAdapter.getPriceByDate(brandId, productId, date, coin ,format);
    }


}
