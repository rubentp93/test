package com.example.inditex.infrastructure.controller.Adapter.implementations;

import com.example.inditex.domain.service.PriceService;
import com.example.inditex.infrastructure.controller.Adapter.PriceServiceFacadeAdapter;
import com.example.inditex.infrastructure.controller.Adapter.mappers.PriceMapper;
import com.example.inditex.infrastructure.controller.Models.ItemPriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PriceServiceFacadeAdapterImpl implements PriceServiceFacadeAdapter {

    @Autowired
    PriceMapper priceMapper;
    @Autowired
    PriceService priceService;

    @Override
    public ItemPriceDTO getPriceByDate(long brandId, long productId, String date, String coin, String format) {
        return priceMapper.fromBoToDto(priceService.getPriceByDate(brandId, productId, date , coin, format));
    }


}
