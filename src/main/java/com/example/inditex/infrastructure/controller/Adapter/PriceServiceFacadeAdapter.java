package com.example.inditex.infrastructure.controller.Adapter;

import com.example.inditex.infrastructure.controller.Models.ItemPriceDTO;

public interface PriceServiceFacadeAdapter {

    ItemPriceDTO getPriceByDate(long brandId, long productId, String date, String coin, String format) ;

}
