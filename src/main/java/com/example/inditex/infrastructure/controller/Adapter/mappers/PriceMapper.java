package com.example.inditex.infrastructure.controller.Adapter.mappers;

import com.example.inditex.domain.models.PriceBO;
import com.example.inditex.infrastructure.controller.Models.ItemPriceDTO;
import org.springframework.stereotype.Component;

@Component
public class PriceMapper {

    public ItemPriceDTO fromBoToDto(PriceBO priceBO) { //TODO REPLACE THIS FOR MAPSTRUCT CLASS
        return ItemPriceDTO.builder()
                .productId(priceBO.getProductId())
                .brandId(priceBO.getBrandId())
                .startTime(priceBO.getStartTime().getEpochSecond())
                .endTime(priceBO.getEndTime().getEpochSecond())
                .price(priceBO.getPrice()).build();
    }

}
