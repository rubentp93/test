package com.example.inditex.infrastructure.controller.Models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class ItemPriceDTO {
    long productId;
    long brandId ;
    long startTime;
    long priceListIId;
    long endTime;;//We should use time stamp in international applications.
    BigDecimal price;// We should use big decimal for not lose decimals when we sum or rest



}
