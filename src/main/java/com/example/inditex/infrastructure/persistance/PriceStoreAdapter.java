package com.example.inditex.infrastructure.persistance;


import com.example.inditex.domain.models.PriceBO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceStoreAdapter  {

    List<PriceBO> getPrices(long brandId, long productId, String coin );

}
