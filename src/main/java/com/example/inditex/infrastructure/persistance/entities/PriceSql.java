package com.example.inditex.infrastructure.persistance.entities;

import com.example.inditex.domain.models.PriceBO;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
//1592121600  1592143200  1592161200 1592208000 1592334000
@Entity
@Data
@Table(name="prices")
public class PriceSql {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    BigDecimal price;
    long productId;
    long brandId;
    int priority;
    long priceListId;
    Timestamp startTime;
    Timestamp endTime;
    String currency;



    //  INSERT INTO PRICES (ID,BRAND_ID ,CURRENCY ,END_TIME ,PRICE ,PRIORITY ,PRODUCT_ID ,START_TIME ) VALUES (2,1,'EUR',NOW(),1,112,1,NOW())
    //  INSERT INTO PRICES (ID ,BRAND_ID ,CURRENCY ,END_TIME ,PRICE ,PRIORITY ,PRODUCT_ID ,START_TIME ) VALUES (0,1,'EUR',NOW(),10,111,1,NOW())
    public PriceBO toBo() {
        return PriceBO.builder()
                .brandId(this.brandId)
                .productId(this.productId)
                .price(this.price)
                .priceListId(this.priceListId)
                .priority(this.priority)
                .startTime(this.startTime.toInstant())
                .endTime(this.endTime.toInstant())
                .build();
    }

    public void toDb(PriceBO priceBO){
        id = null;
        price = priceBO.getPrice();
        priceListId = priceBO.getPriceListId();
        productId = priceBO.getProductId();
        brandId = priceBO.getBrandId();
        priority = priceBO.getPriority();
        startTime = Timestamp.from(priceBO.getStartTime());
        endTime = Timestamp.from(priceBO.getEndTime());
    }

}
