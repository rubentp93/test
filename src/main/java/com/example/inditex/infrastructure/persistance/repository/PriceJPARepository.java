package com.example.inditex.infrastructure.persistance.repository;

import com.example.inditex.infrastructure.persistance.entities.PriceSql;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PriceJPARepository extends JpaRepository<PriceSql, Long> {

    List<PriceSql> findByBrandIdAndProductIdAndCurrency(Long creatorId, Long destinatorId, String currency);

}
