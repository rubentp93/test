package com.example.inditex.infrastructure.persistance.implementation;


import com.example.inditex.domain.models.PriceBO;
import com.example.inditex.infrastructure.persistance.PriceStoreAdapter;
import com.example.inditex.infrastructure.persistance.entities.PriceSql;
import com.example.inditex.infrastructure.persistance.repository.PriceJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PriceStoreAdapterImpl implements PriceStoreAdapter {

    @Autowired
    PriceJPARepository priceRepository;

    public List<PriceBO> getPrices( long brandId, long productId, String coin ) {
        return priceRepository.findByBrandIdAndProductIdAndCurrency(brandId, productId, coin)
                .stream().map(PriceSql::toBo).collect(Collectors.toList());
    }

}
