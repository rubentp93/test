package com.example.inditex;

import com.example.inditex.domain.exceptions.NotFoundException;
import com.example.inditex.domain.models.PriceBO;
import com.example.inditex.domain.service.PriceService;
import com.example.inditex.domain.service.implemtents.PriceServiceImpl;
import com.example.inditex.infrastructure.persistance.PriceStoreAdapter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@Import({PriceServiceImpl.class})
class PriceServiceTest {


	@Autowired
	PriceService priceService;

	@MockBean
	PriceStoreAdapter priceStoreAdapter;

	@Test
	void getProductPriceByMomentComplexTest() {
		//GIVEN
		PriceBO priceBoMock1 = PriceBO.builder().price(BigDecimal.valueOf(100)).priority(1)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(3000))
				.build();

		PriceBO priceBoMock2 = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(2)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(2000))
				.build();

		PriceBO priceBoMock3 = PriceBO.builder().price(BigDecimal.valueOf(300)).priority(3)
				.startTime(Instant.ofEpochSecond(500)).endTime(Instant.ofEpochSecond(1500))
				.build();

		PriceBO expectedPriceBo = PriceBO.builder().price(BigDecimal.valueOf(400)).priority(4)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(1500))
				.build();

		PriceBO priceBoMock4 = PriceBO.builder().price(BigDecimal.valueOf(500)).priority(5)
				.startTime(Instant.ofEpochSecond(0)).endTime(Instant.ofEpochSecond(100))
				.build();

		List<PriceBO> priceListMock = List.of(priceBoMock1, priceBoMock2, priceBoMock3, expectedPriceBo, priceBoMock4);
		when(priceStoreAdapter.getPrices(anyLong(),anyLong(), anyString()))
				.thenReturn(priceListMock);
		//WHEN
		PriceBO result = priceService.getPriceByDate(1,1, "1100","EUR","epoc");

		//THEN
		assertThat(result).usingRecursiveComparison().isEqualTo(expectedPriceBo);
	}

	@Test
	void getProductPriceByMomentSameMomentTest() {
		//GIVEN
		PriceBO expectedPriceBo = PriceBO.builder().price(BigDecimal.valueOf(100)).priority(10)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(1500))
				.build();

		PriceBO priceBoMock2 = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(1)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(1500))
				.build();


		List<PriceBO> priceListMock = List.of(expectedPriceBo, priceBoMock2);
		when(priceStoreAdapter.getPrices(anyLong(),anyLong(), anyString()))
				.thenReturn(priceListMock);
		//WHEN
		PriceBO result = priceService.getPriceByDate(1,1, "1100","EUR","epoc");

		//THEN
		assertThat(result).usingRecursiveComparison().isEqualTo(expectedPriceBo);
	}

	@Test
	void getProductPriceByMomentDIffMomentTest() {
		//GIVEN
		PriceBO priceBoMock1 = PriceBO.builder().price(BigDecimal.valueOf(100)).priority(10)
				.startTime(Instant.ofEpochSecond(0)).endTime(Instant.ofEpochSecond(1000))
				.build();

		PriceBO expectedPriceBo = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(10)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(2000))
				.build();


		List<PriceBO> priceListMock = List.of(priceBoMock1, expectedPriceBo);
		when(priceStoreAdapter.getPrices(anyLong(),anyLong(), anyString()))
				.thenReturn(priceListMock);
		//WHEN
		PriceBO result = priceService.getPriceByDate(1,1, "1100","EUR","epoc");

		//THEN
		assertThat(result).usingRecursiveComparison().isEqualTo(expectedPriceBo);
	}

	@Test
	void getProductPriceByMomentNotFoundTest() {
		//GIVEN
		PriceBO priceBoMock = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(10)
				.startTime(Instant.ofEpochSecond(1)).endTime(Instant.ofEpochSecond(99))
				.build();

		PriceBO priceBo1Mock = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(10)
				.startTime(Instant.ofEpochSecond(101)).endTime(Instant.ofEpochSecond(2000))
				.build();

		List<PriceBO> priceListMock = List.of(priceBoMock, priceBo1Mock);
		when(priceStoreAdapter.getPrices(anyLong(),anyLong(), anyString())).thenReturn(priceListMock);

		//WHEN //THEN
		assertThrows(NotFoundException.class, () -> {
			priceService.getPriceByDate(1,1, "100","EUR","epoc");
		});
	}

	@Test
	void getProductPriceByDateTest() throws ParseException {

		//GIVEN
		PriceBO priceBoMock1 = PriceBO.builder().price(BigDecimal.valueOf(100)).priority(1)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(3000))
				.build();

		PriceBO priceBoMock2 = PriceBO.builder().price(BigDecimal.valueOf(200)).priority(2)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(2000))
				.build();

		PriceBO priceBoMock3 = PriceBO.builder().price(BigDecimal.valueOf(300)).priority(3)
				.startTime(Instant.ofEpochSecond(500)).endTime(Instant.ofEpochSecond(1500))
				.build();

		PriceBO expectedPriceBo = PriceBO.builder().price(BigDecimal.valueOf(400)).priority(4)
				.startTime(Instant.ofEpochSecond(1000)).endTime(Instant.ofEpochSecond(1500))
				.build();

		PriceBO priceBoMock4 = PriceBO.builder().price(BigDecimal.valueOf(500)).priority(5)
				.startTime(Instant.ofEpochSecond(0)).endTime(Instant.ofEpochSecond(700))
				.build();

		List<PriceBO> priceListMock = List.of(priceBoMock1, priceBoMock2, priceBoMock3, expectedPriceBo, priceBoMock4);
		when(priceStoreAdapter.getPrices(anyLong(),anyLong(), anyString()))
				.thenReturn(priceListMock);
		//WHEN

		LocalDateTime datetime = LocalDateTime.ofInstant(Instant.ofEpochSecond(1200), ZoneOffset.UTC);
		String formatted = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss").format(datetime);
		PriceBO result = priceService.getPriceByDate(1,1, formatted,"EUR","date");
		//THEN
		assertThat(result).usingRecursiveComparison().isEqualTo(expectedPriceBo);
	}



}
