package com.example.inditex;


import com.example.inditex.infrastructure.controller.Models.ItemPriceDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class PriceControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void acceptanceTest() throws Exception {

        ItemPriceDTO expected = ItemPriceDTO.builder().productId(1).price(BigDecimal.ONE).brandId(1).startTime(2232).endTime(21321).build();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/price/brands/1/products/35455?date=1592334000&coin=EUR")
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.price").value("38.95"))
                .andReturn();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/price/brands/1/products/35455?date=1592143200&coin=EUR")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.price").value("25.45"))
                .andReturn();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/price/brands/1/products/35455?date=1592156120&coin=EUR")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.price").value("35.5"))
                .andReturn();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/price/brands/1/products/35455?date=1592208000&coin=EUR")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.price").value("30.5"))
                .andReturn();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/price/brands/1/products/35455?date=1592334000&coin=EUR")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.price").value("38.95"))
                .andReturn();




    }

}