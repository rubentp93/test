# PRICE SERVICE IN SPRING BOOT

## Overview
The following repo contains an implementation in Spring Boot of the price service for Inditex .

## Guidelines
For run the application follow the next steps in cmd:

1. Clone this repository:
git clone ....

2. Build the application and install all dependencies:
mv clean install

3. Run the app executing:
java -jar  target/app-x.x.x-SNAPSHOT.jar

## TEST REST SERVICE
    We execute an SQL file (resources/date.sql) when we start the app with the data provided as example.

## HTTP GET GetPrice
### Examples:
     example-url-epoc = http://localhost:8080/brands/{brandId}/products/{productId}?date=2342523&coin=EUR&format=epoc
     example-url-date = http://localhost:8080/brands/{brandId}/products/{productId}?date=2000-01-14-11.30.34&coin=EUR&format=date
     example-url-default = http://localhost:8080/brands/{brandId}/products/{productId}?date=2342523
### Query Params Explanation
       number brandId (REQUIRED)
       number productId (REQUIRED)
       String date (REQUIRED) could be in epoc or date
       String coin (OPTIONAL) (defaultValue = "EUR")
       String format (OPTIONAL) (defaultValue = "epoc")
            (options=[
                "epoc":" Epoch Unix Timestamp in seconds. Number of seconds since JAN 01 1970. (UTC),
                "date":" Date in format yyyy-MM-dd-HH.mm.ss (UTC)"
                ])

       ERROR 404 : In case the price is not found, the server will return HTTP-404 NOT_FOUND error.
       ERROR 400 : In case the date, coin or format are not valid, the server returns HTTP-400 Bad Request error.

## TEST
For test the app is working as expected, please run the unit test and integration test added:

### UNIT TEST
#### 1 PriceServiceTest.java
In this case we are testing the behaviour of the service mocking its dependencies.

##### TEST 1: getProductPriceByMomentComplexTest
we are testing the happy flow (we have 5 price with the same or different periods of time and different priorities)
and we check the result is how we expect.


### INTEGRATION TEST PriceControllerIT.java
It tests the whole application from controller to db.